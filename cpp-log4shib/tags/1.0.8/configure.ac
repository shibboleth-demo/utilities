AC_INIT(log4shib, 1.0.8)

# autoconf 2.50 or higher to rebuild aclocal.m4, because the
# AC_CREATE_PREFIX_CONFIG_H macro needs the AS_DIRNAME macro.
AC_PREREQ(2.50)

#
# +1 : ? : +1  == new interface that does not break old one
# +1 : ? : 0   == new interface that breaks old one
#  ? : ? : 0   == no new interfaces, but breaks apps
#  ? :+1 : ?   == just some internal changes, nothing breaks but might work 
#                 better
# CURRENT : REVISION : AGE
LT_VERSION=1:8:0

AC_SUBST(LT_VERSION)

#AC_CONFIG_SRCDIR(configure.in)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_HEADERS([include/config.h])
AX_PREFIX_CONFIG_H([include/log4shib/config.h])
AC_CONFIG_MACRO_DIR(m4)
AM_INIT_AUTOMAKE

# General "with" options
# ----------------------------------------------------------------------------
AC_ARG_WITH(idsa, [  --with-idsa             include idsa support])

# Checks for programs
# ----------------------------------------------------------------------------
AC_CANONICAL_HOST

AC_ARG_ENABLE(debug,
    AC_HELP_STRING(--enable-debug, [Have GCC compile with symbols (Default = no)]),
    enable_debug=$enableval, enable_debug=no)

if test "$enable_debug" = "yes" ; then
    GCC_CFLAGS="$CFLAGS -g -D_DEBUG"
    GCC_CXXFLAGS="$CXXFLAGS -g -D_DEBUG"
else
    GCC_CFLAGS="$CFLAGS -O2 -DNDEBUG"
    GCC_CXXFLAGS="$CXXFLAGS -O2 -DNDEBUG"
fi

AM_PROG_LIBTOOL
AC_PROG_INSTALL
AC_PROG_MAKE_SET

AC_PROG_CXX([g++ c++ gpp aCC CC cxx cc++ cl FCC KCC RCC xlC_r xlC QCC])

if test "$GCC" = "yes" ; then
    CFLAGS="-Wall $GCC_CFLAGS"
    CXXFLAGS="-Wall $GCC_CXXFLAGS"
fi

# Enables POSIX semantics for some functions.
case "${host_cpu}-${host_os}" in
    *solaris*)
        CFLAGS="$CFLAGS -D_POSIX_PTHREAD_SEMANTICS"
        CXXFLAGS="$CXXFLAGS -D_POSIX_PTHREAD_SEMANTICS"
    ;;
esac

AC_PROG_CXXCPP
AC_LANG(C)

# Checks header files
# ----------------------------------------------------------------------------
AC_CHECK_HEADERS([unistd.h])
AC_CHECK_HEADERS([io.h])


# Checks close-on-exec functionality
# ----------------------------------------------------------------------------
AC_CHECK_DECL([O_CLOEXEC],
        [AC_DEFINE([HAVE_O_CLOEXEC],[1],[Define to 1 if open supports O_CLOEXEC flag.])],,
        [#include <fcntl.h>])
AC_CHECK_DECL([FD_CLOEXEC],
        [AC_DEFINE([HAVE_FD_CLOEXEC],[1],[Define to 1 if fcntl supports FD_CLOEXEC flag.])],,
        [#include <fcntl.h>])
AC_CACHE_CHECK([for SOCK_CLOEXEC support], [log_cv_sock_cloexec],
[AC_TRY_RUN([
#include <sys/types.h>
#include <sys/socket.h>
int main()
{
return socket(AF_INET, SOCK_STREAM|SOCK_CLOEXEC, 0) == -1;
}], [log_cv_sock_cloexec=yes], [log_cv_sock_cloexec=no], [log_cv_sock_cloexec=no])])

if test "$log_cv_sock_cloexec" = "yes"; then
	AC_DEFINE([HAVE_SOCK_CLOEXEC], 1, [Define if the SOCK_CLOEXEC flag is supported])
fi


# Checks local idioms
# ----------------------------------------------------------------------------
AC_C_INT64_T
AC_FUNC_SNPRINTF
AC_CHECK_FUNCS([syslog gettimeofday ftime localtime_r gmtime_r])

# Checks for libraries
# ----------------------------------------------------------------------------

# for RemoteSyslogAppender
AC_CHECK_LIB(socket,socket, LIBS="-lsocket $LIBS",,)
AC_CHECK_LIB(nsl,gethostbyname, LIBS="-lnsl $LIBS",,)

# checks for pthreads
AX_PTHREAD([enable_threads="pthread"],[enable_threads="no"])
if test $enable_threads != "pthread"; then
    AC_MSG_ERROR([unable to find pthreads, currently this is required])
else
    AC_DEFINE(HAVE_PTHREAD,1,[Define if you have POSIX threads libraries and header files.])
    AC_DEFINE(HAVE_THREADING,1,[define if threading is enabled])
    AC_DEFINE(USE_PTHREADS,1,[define if pthread library is available])
    AC_DEFINE(_PTHREADS,1,[define for STL if pthread library is used])
    LIBS="$PTHREAD_LIBS $LIBS"
    CFLAGS="$PTHREAD_CFLAGS $CFLAGS"
    CXXFLAGS="$PTHREAD_CFLAGS $CXXFLAGS"
    CC="$PTHREAD_CC"
fi

AC_LANG(C++)

AC_CXX_HAVE_SSTREAM

# idsa_test
if test "x$with_idsa" = xyes; then
    AC_CHECK_LIB([idsa], [idsa_open])
    if test "$ac_cv_lib_idsa_idsa_open" = no; then
        AC_MSG_ERROR([could not locate idsa library])
    fi
fi

# check for doxygen
# ----------------------------------------------------------------------------
BB_ENABLE_DOXYGEN

# check for omnithreads
#BB_CHECK_OMNITHREADS
#BB_CHECK_PTHREADS

LOG4SHIB_CFLAGS="$CXXFLAGS"
LOG4SHIB_LIBS="-llog4shib"
LOG4SHIB_LIBDEPS="$LIBS"
LOG4SHIB_VERSION="$VERSION"

PETI_PEDANTIC_GCC

# Create files
# ----------------------------------------------------------------------------

AC_CONFIG_LIBCONFIG_IN([log4shib])
AC_CONFIG_PKGCONFIG_IN([log4shib], [C++ library for flexible logging, modeled after Log4j])

AC_CONFIG_FILES([
Makefile
log4shib.spec
log4shib.pc
log4shib-config
Portfile
config/Makefile
doc/Makefile
doc/Doxyfile
doc/html/Makefile
src/Makefile
include/Makefile
include/log4shib/Makefile
include/log4shib/threading/Makefile
tests/Makefile
debian/Makefile
msvc6/Makefile
msvc6/log4shib/Makefile
msvc6/log4shibDLL/Makefile
msvc6/testCategory/Makefile
msvc6/testDLL/Makefile
msvc6/testMain/Makefile
msvc6/testNDC/Makefile
msvc6/testNTEventLog/Makefile
msvc6/testPattern/Makefile
msvc8/Makefile
msvc8/log4shib/Makefile
msvc8/log4shibDLL/Makefile
msvc8/testCategory/Makefile
msvc8/testDLL/Makefile
msvc8/testMain/Makefile
msvc8/testNDC/Makefile
msvc8/testNTEventLog/Makefile
msvc8/testPattern/Makefile
msvc9/Makefile
msvc9/log4shib/Makefile
msvc9/log4shibDLL/Makefile
msvc9/testCategory/Makefile
msvc9/testDLL/Makefile
msvc9/testMain/Makefile
msvc9/testNDC/Makefile
msvc9/testNTEventLog/Makefile
msvc9/testPattern/Makefile
msvc10/Makefile
msvc10/log4shib/Makefile
msvc10/log4shibDLL/Makefile
msvc10/testCategory/Makefile
msvc10/testMain/Makefile
msvc10/testNDC/Makefile
msvc10/testNTEventLog/Makefile
msvc10/testPattern/Makefile
bcb5/Makefile
bcb5/log4shib/Makefile
bcb5/testCategory/Makefile
bcb5/testConfig/Makefile
bcb5/testFixedContextCategory/Makefile
bcb5/testmain/Makefile
bcb5/testNDC/Makefile
bcb5/testPattern/Makefile
openvms/Makefile
])
AC_OUTPUT
