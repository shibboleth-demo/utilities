Changes in Release 1.1.4
=============================================
[XTSJ-10] - Correct class and command name
[XSTJ-9] - Add support for reading/writting base64, deflate, gzip encoded files
[XTSJ-8] - xmlsectool generates spurious xmlns:xml definitions in output
[XTSJ-7] - verify xmlsectool dependencies

Changes in Release 1.1.3
=============================================
[XSTJ-6] - program fails with a NullPointerException when using a signing key from the filesystem without a password

Changes in Release 1.1.2
=============================================
[XSTJ-1] - Update dependency libraries for version 1.1.2
[XSTJ-2] - non-zero status code not returned when a signature is invalid