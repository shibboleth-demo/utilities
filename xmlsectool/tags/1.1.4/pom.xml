<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>net.shibboleth.tool</groupId>
	<artifactId>xmlsectool</artifactId>
	<version>1.1.4</version>
	<packaging>jar</packaging>

	<name>XmlSecTool</name>
	<description>
        A command line tool for checking an XML file for well-formedness and validity as well as 
        signing and checking signatures.
    </description>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <slf4j.groupId>org.slf4j</slf4j.groupId>
        <slf4j.version>1.6.1</slf4j.version>
        <xerces.groupId>org.apache.xerces</xerces.groupId>
        <xerces.version>2.10.0</xerces.version>
        <xalan.groupId>xalan</xalan.groupId>
        <xalan.version>2.7.1</xalan.version>
    </properties>

    <repositories>
        <repository>
            <id>shib-release</id>
            <url>https://shibboleth.net/nexus/content/groups/public</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>shib-snapshot</id>
            <url>https://shibboleth.net/nexus/content/repositories/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
        </repository>
    </repositories>

	<dependencies>
		<!-- Compile dependencies -->
		<dependency>
			<groupId>org.opensaml</groupId>
			<artifactId>openws</artifactId>
			<version>1.4.2</version>
            <exclusions>
                <exclusion>
                    <groupId>commons-logging</groupId>          
                    <artifactId>commons-logging</artifactId>            
                </exclusion>
                <exclusion>
                    <groupId>velocity</groupId>
                    <artifactId>velocity</artifactId>
                </exclusion>            
            </exclusions>
		</dependency>
        <dependency>
            <groupId>edu.internet2.middleware</groupId>
            <artifactId>shibboleth-jce</artifactId>
            <version>1.1.0</version>
        </dependency>
		<dependency>
			<groupId>${slf4j.groupId}</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>${slf4j.version}</version>
		</dependency>
		<dependency>
			<groupId>${slf4j.groupId}</groupId>
			<artifactId>log4j-over-slf4j</artifactId>
			<version>${slf4j.version}</version>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>0.9.27</version>
		</dependency>
        <dependency>
            <groupId>jargs</groupId>
            <artifactId>jargs</artifactId>
            <version>1.0</version>
        </dependency>

    	<!-- Runtime dependencies -->
        <dependency>
            <groupId>${xerces.groupId}</groupId>
            <artifactId>xml-apis</artifactId>
            <version>${xerces.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>${xerces.groupId}</groupId>
            <artifactId>xercesImpl</artifactId>
            <version>${xerces.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>${xerces.groupId}</groupId>
            <artifactId>serializer</artifactId>
            <version>${xerces.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>${xalan.groupId}</groupId>
            <artifactId>xalan</artifactId>
            <version>${xalan.version}</version>
            <scope>runtime</scope>
            <exclusions>
                <exclusion>
                    <groupId>xalan</groupId>
                    <artifactId>serializer</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
	</dependencies>

    <distributionManagement>
        <repository>
            <id>release</id>
            <url>https://shibboleth.net/nexus/content/repositories/releases</url>
        </repository>
        <snapshotRepository>
            <id>snapshot</id>
            <url>https://shibboleth.net/nexus/content/repositories/snapshots</url>
        </snapshotRepository>
    </distributionManagement>
    
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
				<configuration>
					<source>1.5</source>
					<target>1.5</target>
					<debug>true</debug>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
                <version>2.2.1</version>
				<configuration>
					<descriptors>
						<descriptor>src/main/assembly/bin.xml</descriptor>
					</descriptors>
				</configuration>
				<executions>
					<execution>
						<id>make-assembly</id>
						<phase>package</phase>
						<goals>
							<goal>attached</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>2.1.2</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>2.7</version>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <links>
                        <link>http://java.sun.com/j2se/1.5.0/docs/api/</link>
                    </links>
                    <quiet>true</quiet>
                    <author>false</author>
                    <version>true</version>
                    <doctitle>${project.name} ${project.version} Java API</doctitle>
                    <windowtitle>${project.name} ${project.version} Java API</windowtitle>
                </configuration>
            </plugin>
		</plugins>
	</build>

    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.2</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
    

	<!-- Project Metadata -->
	<url>http://shibboleth.net/</url>

	<inceptionYear>2009</inceptionYear>
	<licenses>
		<license>
			<name>Apache 2</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<organization>
		<name>Internet2</name>
		<url>http://www.internet2.edu/</url>
	</organization>

	<issueManagement>
		<system>JIRA</system>
		<url>http://issues.shibboleth.net/</url>
	</issueManagement>

	<mailingLists>
		<mailingList>
			<name>Shibboleth Announce</name>
			<subscribe>http://shibboleth.internet2.edu/support.html#lists
			</subscribe>
			<unsubscribe>http://shibboleth.internet2.edu/support.html#lists
			</unsubscribe>
			<post>shibboleth-announce@internet2.edu</post>
			<archive>https://mail.internet2.edu/wws/arc/shibboleth-announce
			</archive>
		</mailingList>
		<mailingList>
			<name>Shibboleth Users</name>
			<subscribe>http://shibboleth.internet2.edu/support.html#lists
			</subscribe>
			<unsubscribe>http://shibboleth.internet2.edu/support.html#lists
			</unsubscribe>
			<post>shibboleth-users@internet2.edu</post>
			<archive>https://mail.internet2.edu/wws/arc/shibboleth-users
			</archive>
		</mailingList>
		<mailingList>
			<name>Shibboleth Development</name>
			<subscribe>http://shibboleth.internet2.edu/support.html#lists
			</subscribe>
			<unsubscribe>http://shibboleth.internet2.edu/support.html#lists
			</unsubscribe>
			<post>shibboleth-dev@internet2.edu</post>
			<archive>https://mail.internet2.edu/wws/arc/shibboleth-dev</archive>
		</mailingList>
	</mailingLists>

	<scm>
		<connection>scm:svn:https://svn.shibboleth.net/utilities/xmlsectool</connection>
		<developerConnection>scm:svn:https://svn.shibboleth.net/utilities/xmlsectool
		</developerConnection>
		<tag>HEAD</tag>
		<url>http://svn.shibboleth.net/view/utilities/xmlsectool</url>
	</scm>

</project>